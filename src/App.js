import React, { Component } from "react";
import "./App.css";
import ReduxFormWelcomeLogin from "./Views/ReduxForm/ReduxFormWelcomeLogin";
import ReduxFormStartEndDay from "./Views/ReduxForm/ReduxFormStartEndDay";
import ReduxFormStartEndBreak from "./Views/ReduxForm/ReduxFormStartEndBreak";
import ReduxFormBreakInProgress from "./Views/ReduxForm/ReduxFormBreakInProgress";

import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={() => <h1>Welcometo home</h1>} />
          <Route
            exact
            path="/ReduxFormWelcomeLogin"
            component={ReduxFormWelcomeLogin}
          />
          <Route
            exact
            path="/ReduxFormStartEndDay"
            component={ReduxFormStartEndDay}
          />
          <Route
            exact
            path="/ReduxFormStartEndBreak"
            component={ReduxFormStartEndBreak}
          />
          <Route
            exact
            path="/ReduxFormBreakInProgress"
            component={ReduxFormBreakInProgress}
          />
        </Switch>
      </Router>
    );
  }
}

export default App;
