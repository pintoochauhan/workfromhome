import React, { Component } from "react";
import "./ReduxFormCss.css";
import Grid from "@material-ui/core/Grid";
import { TextField, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import Background from "../../images/Attendance-Management-background.png";
import PersonOutlineIcon from "@material-ui/icons/PersonOutline";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import HttpIcon from "@material-ui/icons/Http";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import axios from "axios";

class ReduxFormWelcomeLogin extends Component {
  state = {
    name: null,
    buttonLinkn: "#"
  };
  componentDidUpdate() {
    console.log("CDM : this.state", this.state);
    // if (this.buttonLinknn == "/ReduxFormStartEndDay") {
    //   const buttonLinknnn = this.buttonLinknn;
    //   console.log("buttonLinknnn : ", buttonLinknnn);
    //   this.setState(state => {
    //     return {
    //       ...state,
    //       buttonLinkn: buttonLinknnn
    //     };
    //   });
    // }
  }
  usernameTextField = e => {
    const name = e.target.name;
    const value = e.target.value;
    console.log(name, value);
    this.setState(state => {
      return {
        ...state,
        [name]: value
      };
    });
  };
  userpassTextField = e => {
    const name = e.target.name;
    const value = e.target.value;
    console.log(name, value);
    this.setState(state => {
      return {
        ...state,
        [name]: value
      };
    });
  };
  submitButton = () => {
    if (this.state.username == "pintoo" && this.state.userpass == "12345") {
      console.log("submit");
      const buttonLinknn = "/ReduxFormStartEndDay";
      console.log("buttonLinknn : ", buttonLinknn);

      this.setState(state => {
        return {
          ...state,
          buttonLinkn: buttonLinknn
        };
      });
    }
  };
  render() {
    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{
          height: "100vh",
          background: `url(${Background}) no-repeat`
        }}
      >
        <Grid item style={{ textAlign: "center", marginBottom: "30px" }}>
          <h1>Welcome to Work From Home App </h1>
          <h1>Please ! Login to Start Work</h1>
        </Grid>
        <Grid item>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            style={{
              border: "6px solid orange",
              borderRadius: "15px",
              padding: "40px"
            }}
          >
            <Grid
              item
              className="TextField-Grid"
              style={{ marginBottom: "30px" }}
            >
              <span>
                <PersonOutlineIcon />
              </span>
              <TextField
                id="outlined-basic"
                label="Username"
                variant="outlined"
                name="username"
                onChange={this.usernameTextField}
              />
            </Grid>
            <Grid
              item
              className="TextField-Grid"
              style={{ marginBottom: "30px" }}
            >
              <span>
                <LockOpenIcon />
              </span>
              <TextField
                id="outlined-basic"
                label="Password"
                variant="outlined"
                name="userpass"
                onChange={this.userpassTextField}
              />
            </Grid>
            <Grid
              item
              className="TextField-Grid"
              style={{ marginBottom: "30px" }}
            >
              <span>
                <HttpIcon />
              </span>
              <TextField
                id="outlined-basic"
                label="URL"
                variant="outlined"
                name=""
                disabled
              />
            </Grid>
            <Grid
              item
              className="TextField-Grid"
              style={{ marginBottom: "30px" }}
            >
              <Link to={this.state.buttonLinkn} name="buttonLinkName">
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.submitButton}
                >
                  Submit
                  <span>
                    <NavigateNextIcon />
                  </span>
                </Button>
              </Link>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default ReduxFormWelcomeLogin;
