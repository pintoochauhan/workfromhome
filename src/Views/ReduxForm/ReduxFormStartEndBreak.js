import React, { Component } from "react";
import { Grid, TextField, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import Background from "../../images/V3fMF8.png";
import "./ReduxFormCss.css";

class ReduxFormStartEndBreak extends Component {
  render() {
    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{
          height: "100vh",
          background: `url(${Background}) no-repeat`
        }}
      >
        <Grid item style={{ marginBottom: "50px" }}>
          <Grid container direction="row" justify="center" alignItems="center">
            <Grid item xs={5} sm={5} md={5} lg={5} xl={5}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid
                  item
                  xs={5}
                  sm={5}
                  md={5}
                  lg={5}
                  xl={5}
                  style={{
                    width: "20%",
                    height: "20%",
                    marginBottom: "20px",
                    border: "2px solid orange",
                    borderRadius: "50%",

                    padding: "12px"
                  }}
                >
                  <img
                    src={require("../../images/PinClipart.com_rooster-weathervane-clipart_4989937.png")}
                    style={{ width: "100%" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={5}
                  sm={5}
                  md={5}
                  lg={5}
                  xl={5}
                  className="StartEndBreakTextField"
                >
                  <TextField
                    id="outlined-basic"
                    label="Enter Start Time"
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid
              item
              xs={2}
              sm={2}
              md={2}
              lg={2}
              xl={2}
              style={{ textAlign: "center" }}
            >
              <h1 style={{ color: "#fff" }}>To</h1>
            </Grid>
            <Grid item xs={5} sm={5} md={5} lg={5} xl={5}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid
                  item
                  xs={5}
                  sm={5}
                  md={5}
                  lg={5}
                  xl={5}
                  style={{
                    width: "20%",
                    height: "20%",
                    marginBottom: "20px",
                    border: "2px solid orange",
                    borderRadius: "50%",

                    padding: "12px"
                  }}
                >
                  <img
                    src={require("../../images/Hourglass.png")}
                    style={{ width: "100%" }}
                  />
                </Grid>
                <Grid item className="StartEndBreakTextField">
                  <TextField
                    id="outlined-basic"
                    label="Enter End Time"
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item style={{ marginBottom: "50px" }}>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid
              item
              xs={6}
              sm={6}
              md={6}
              lg={6}
              xl={6}
              style={{
                width: "50%",
                height: "20%",
                marginBottom: "20px",
                border: "2px solid orange",
                borderRadius: "50%",

                padding: "12px"
              }}
            >
              <img
                src={require("../../images/reason-icon-png-7.png")}
                style={{ width: "100%" }}
              />
            </Grid>
            <Grid
              item
              xs={12}
              sm={12}
              md={12}
              lg={12}
              xl={12}
              className="StartEndBreakTextField"
            >
              <TextField
                id="outlined-basic"
                label="Please ! Give Me a Reason"
                variant="outlined"
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Link
            to="/ReduxFormBreakInProgress"
            style={{ textDecoration: "none" }}
          >
            <Button variant="contained" color="primary">
              Start Break
            </Button>
          </Link>
        </Grid>
      </Grid>
    );
  }
}

export default ReduxFormStartEndBreak;
