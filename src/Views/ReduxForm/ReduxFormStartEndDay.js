import React, { Component } from "react";
import { Grid, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import Background from "../../images/V3fMF8.png";

class ReduxFormStartEndDay extends Component {
  state = {
    date: new Date(),
    count: 0
  };
  // https://www.youtube.com/watch?v=ZAXuxemjQXQ
  callMe() {
    setInterval(() => {
      this.setState(state => {
        return {
          ...state,
          date: new Date()
        };
      });
    }, 1000);
  }

  render() {
    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{
          height: "100vh",
          background: `url(${Background}) no-repeat`
        }}
      >
        <Grid item>
          <div className="App">
            <h1>Current Time</h1>
            <h2>It is {this.state.date.toLocaleTimeString()}</h2>
            {this.callMe()}
            <h2>{this.state.count}</h2>
          </div>
        </Grid>
        <Grid item style={{ marginBottom: "100px" }}>
          <Grid container direction="row" justify="center" alignItems="center">
            <Grid item xs={4} sm={4} md={4} lg={4} xl={4}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid
                  item
                  xs={5}
                  sm={5}
                  md={5}
                  lg={5}
                  xl={5}
                  style={{
                    width: "20%",
                    height: "20%",
                    marginBottom: "20px",
                    border: "2px solid orange",
                    borderRadius: "50%",

                    padding: "12px"
                  }}
                >
                  <img
                    src={require("../../images/pngkey.com-help-icon-png-2054041.png")}
                    style={{ width: "100%" }}
                  />
                </Grid>
                <Grid item xs={5} sm={5} md={5} lg={5} xl={5}>
                  <p style={{ textAlign: "center", color: "#fff" }}>
                    Click Below To Start Your Work
                  </p>
                </Grid>
                <Grid item xs={5} sm={5} md={5} lg={5} xl={5}>
                  <Button variant="contained" color="primary">
                    Start Work
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={4} sm={4} md={4} lg={4} xl={4}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid
                  item
                  xs={5}
                  sm={5}
                  md={5}
                  lg={5}
                  xl={5}
                  style={{
                    width: "20%",
                    height: "20%",
                    marginBottom: "20px",
                    border: "2px solid orange",
                    borderRadius: "50%",

                    padding: "12px"
                  }}
                >
                  <img
                    src={require("../../images/1-11-512.png")}
                    style={{ width: "100%" }}
                  />
                </Grid>
                <Grid item xs={5} sm={5} md={5} lg={5} xl={5}>
                  <p style={{ textAlign: "center", color: "#fff" }}>
                    Click Below To End Your Work
                  </p>
                </Grid>
                <Grid item xs={5} sm={5} md={5} lg={5} xl={5}>
                  <Button variant="contained" color="primary">
                    End Work
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid
              item
              xs={5}
              sm={5}
              md={5}
              lg={5}
              xl={5}
              style={{
                width: "290px",
                height: "120px",
                marginBottom: "20px",
                border: "2px solid orange",
                borderRadius: "100%",
                padding: "12px"
              }}
            >
              <img
                src={require("../../images/PinClipart.com_take-a-break-clip_4942655.png")}
                style={{ width: "100%", height: "100%" }}
              />
            </Grid>
            <Grid item xs={10} sm={10} md={10} lg={10} xl={10}>
              <p style={{ textAlign: "center", color: "#fff" }}>
                Click Below To Take A Break
              </p>
            </Grid>
            <Grid item xs={10} sm={10} md={10} lg={10} xl={10}>
              <Link
                to="/ReduxFormStartEndBreak"
                style={{ textDecoration: "none" }}
              >
                <Button variant="contained" color="primary">
                  Start Break
                </Button>
              </Link>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default ReduxFormStartEndDay;
