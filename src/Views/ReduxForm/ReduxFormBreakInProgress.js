import React, { Component } from "react";
import { TextField, Grid, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import Background from "../../images/V3fMF8.png";
import "./ReduxFormCss.css";

class ReduxFormBreakInProgress extends Component {
  render() {
    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{ height: "100vh", background: `url(${Background})` }}
      >
        <Grid item style={{ marginBottom: "50px" }}>
          <Grid container direction="row" justify="center" alignItems="center">
            <Grid item xs={5} sm={5} md={5} lg={5} xl={5}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid
                  item
                  xs={5}
                  sm={5}
                  md={5}
                  lg={5}
                  xl={5}
                  style={{
                    width: "20%",
                    height: "20%",
                    marginBottom: "20px",
                    border: "2px solid orange",
                    borderRadius: "50%",

                    padding: "12px"
                  }}
                >
                  <img
                    src={require("../../images/PinClipart.com_rooster-weathervane-clipart_4989937.png")}
                    style={{ width: "100%" }}
                  />
                </Grid>
                <Grid item xs={5} sm={5} md={5} lg={5} xl={5}>
                  <p style={{ color: "#fff", textAlign: "center" }}>
                    Your are taken break on this time.
                  </p>
                </Grid>
                <Grid
                  item
                  xs={5}
                  sm={5}
                  md={5}
                  lg={5}
                  xl={5}
                  className="BreakInProgressTextField"
                >
                  <TextField
                    id="outlined-basic"
                    label="Start Time"
                    variant="outlined"
                    disabled
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={5} sm={5} md={5} lg={5} xl={5}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid
                  item
                  xs={5}
                  sm={5}
                  md={5}
                  lg={5}
                  xl={5}
                  style={{
                    width: "20%",
                    height: "20%",
                    marginBottom: "20px",
                    border: "2px solid orange",
                    borderRadius: "50%",

                    padding: "12px"
                  }}
                >
                  <img
                    src={require("../../images/Hourglass.png")}
                    style={{ width: "100%" }}
                  />
                </Grid>
                <Grid item xs={5} sm={5} md={5} lg={5} xl={5}>
                  <p style={{ color: "#fff", textAlign: "center" }}>
                    Your break over on this time.
                  </p>
                </Grid>
                <Grid
                  item
                  xs={5}
                  sm={5}
                  md={5}
                  lg={5}
                  xl={5}
                  className="BreakInProgressTextField"
                >
                  <TextField
                    id="outlined-basic"
                    label="End Time"
                    variant="outlined"
                    disabled
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Link to="/ReduxFormStartEndDay">
            <Button variant="contained" color="primary">
              End Break
            </Button>
          </Link>
        </Grid>
      </Grid>
    );
  }
}

export default ReduxFormBreakInProgress;
